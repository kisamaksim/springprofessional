package maksim.iakidovich.spring.binfun;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BinMainTest {
    
    @Test
    void binarySearch_defaultScenario() {
        int[] arr = {1, 3, 5, 124, 324, 456, 540, 600, 1056, 10145};
        BinMain binMain = new BinMain();
    
        int result = binMain.binarySearch(arr, 456);
        int expectedResult = 5;
        
        assertEquals(expectedResult, result);
    }
}