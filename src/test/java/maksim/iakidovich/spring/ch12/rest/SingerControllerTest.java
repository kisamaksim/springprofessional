package maksim.iakidovich.spring.ch12.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.runners.JUnit44RunnerImpl;
import org.mockito.internal.runners.JUnit45AndHigherRunnerImpl;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import maksim.iakidovich.spring.ch12.entities.Singer;
import maksim.iakidovich.spring.ch12.services.SingerService;

@RunWith(SpringJUnit4ClassRunner.class)
class SingerControllerTest {
    
    private List<Singer> singers = new ArrayList<>();
    
    @Mock
    private SingerService singerServiceMock;
    
    @InjectMocks
    private SingerController singerController;
    
    @BeforeEach
    private void init() {
//        MockitoAnnotations.initMocks(this);
        
        singers.clear();
        Singer singer = new Singer();
        singer.setId(11L);
        singer.setFirstName("maksim");
        singer.setLastName("ia");
        singers.add(singer);
    }
    
    @Test
    void getAllSingers() {
        when(singerServiceMock.findAll()).thenReturn(singers);
    
        List<Singer> resultedSingers = singerController.getAllSingers();
    
        assertEquals(singers, resultedSingers);
    }
    
    @Test
    void findById() {
    }
    
    @Test
    void addSinger() {
    }
}