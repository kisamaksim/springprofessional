package maksim.iakidovich.spring.ch8.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SingerSummary {
    private String firstName;
    private String lastName;
    private String title;
}
