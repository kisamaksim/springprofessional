package maksim.iakidovich.spring.ch8;

import java.util.Date;
import java.util.GregorianCalendar;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import maksim.iakidovich.spring.ch8.config.JpaConfig;
import maksim.iakidovich.spring.ch8.entities.Singer;
import maksim.iakidovich.spring.ch8.entities.SingerAudit;
import maksim.iakidovich.spring.ch8.services.AlbumService;
import maksim.iakidovich.spring.ch8.services.SingerAuditService;
import maksim.iakidovich.spring.ch8.services.SingerService;
import maksim.iakidovich.spring.ch8.services.impl.SingerSummaryUntypeImpl;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(JpaConfig.class);
        
        SingerService singerService = context.getBean(SingerService.class);
//        System.out.println(singerService.findAll());
    
//        List<Singer> allWithAlbums = singerService.findAllWithAlbums();
//        System.out.println(allWithAlbums);
//
//        System.out.println(singerService.findById(1L));
    
        SingerSummaryUntypeImpl bean = context.getBean(SingerSummaryUntypeImpl.class);
//        bean.displayAllSingerSummary();
//        List<SingerSummary> allSingerSummary = bean.getAllSingerSummary();
//        System.out.println(allSingerSummary);
        
        // ----
//        Singer singer = new Singer();
//        singer.setFirstName("BB");
//        singer.setLastName("King");
//        singer.setBirthDate(new Date(new GregorianCalendar(1940, 8, 16).getTime().getTime()));
//
//        Album album = new Album();
//        album.setTitle("My Kind of Blues");
//        album.setReleaseDate(new Date(new GregorianCalendar(1961, 7, 18).getTime().getTime()));
//        singer.addAlbum(album);
//
//        singerService.save(singer);
//        List<Singer> allWithAlbums1 = singerService.findAllWithAlbums();
//        System.out.println(allWithAlbums1);
//
//        singer.setLastName("HOMEEEEER SIMPSON");
//        singerService.save(singer);
//        List<Singer> allWithAlbums2 = singerService.findAllWithAlbums();
//        System.out.println(allWithAlbums2);
//
//        //--
//
//        Singer singer1 = allWithAlbums2.get(3);
//        singer.removeAlbum(singer.getAlbums().get(0));
//        singerService.save(singer1);
//        List<Singer> allWithAlbums = singerService.findAllWithAlbums();
//        System.out.println(allWithAlbums);
        
        //--
    
//        List<Singer> allWithAlbums = singerService.findAllWithAlbums();
//        System.out.println(allWithAlbums);
//        Singer singer = allWithAlbums.get(0);
//        singerService.delete(singer);
//        System.out.println(singerService.findAllWithAlbums());
//        AlbumService bean1 = context.getBean(AlbumService.class);
//        System.out.println(bean1.findAll());
        
        //--
    
//        List allByNativeQuery = singerService.findAllByNativeQuery();
//        System.out.println(allByNativeQuery);
        
        //--
//        List<Singer> byCriteriaQuery = singerService.findByCriteriaQuery("John", "Mayer");
//        System.out.println(byCriteriaQuery);
        
        //---
    
//        System.out.println(singerService.findAll());
        System.out.println(singerService.findByFirstName("John"));
//        System.out.println(singerService.findByFirstNameAndLastName("John", "Mayer"));
        
        //---
    
//        Singer singer = singerService.findById(1L);
//        AlbumService albumService = context.getBean(AlbumService.class);
//        System.out.println(albumService.findByTitle("From"));
//        System.out.println(albumService.findBySinger(singer));
        
        //---
        SingerAuditService singerAuditService = context.getBean(SingerAuditService.class);
        System.out.println("====" + singerAuditService.findAll());
    
        SingerAudit singerAudit = new SingerAudit();
        singerAudit.setFirstName("BB");
        singerAudit.setLastName("King");
        singerAudit.setBirthDate(new Date(new GregorianCalendar(1940, 8, 16).getTime().getTime()));
        singerAuditService.save(singerAudit);
        System.out.println("====" + singerAuditService.findAll());
        
        singerAudit = singerAuditService.findById(1L);
        System.out.println("====" + singerAudit);
        
        singerAudit.setFirstName("John Cena");
        singerAuditService.save(singerAudit);
        System.out.println("====" + singerAuditService.findAll());
        
        //====[SingerAudit(id=1, version=0, firstName=BB, lastName=King, birthDate=1940-09-16, createdBy=ONE PUNCH, lastModifiedBy=ONE PUNCH, lastModifiedDate=2019-06-04 22:52:34.218)]
    
    }
}
