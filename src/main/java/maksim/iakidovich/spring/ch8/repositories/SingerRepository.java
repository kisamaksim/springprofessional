package maksim.iakidovich.spring.ch8.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import maksim.iakidovich.spring.ch8.entities.Singer;

public interface SingerRepository extends JpaRepository<Singer, Long> {
    @Override
    List<Singer> findAll();
    List<Singer> findByFirstName(String firstName);
    List<Singer> findByFirstNameAndLastName(String firstName, String lastName);
}
