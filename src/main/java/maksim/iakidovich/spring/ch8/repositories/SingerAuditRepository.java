package maksim.iakidovich.spring.ch8.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import maksim.iakidovich.spring.ch8.entities.SingerAudit;

public interface SingerAuditRepository extends JpaRepository<SingerAudit, Long> {
}
