package maksim.iakidovich.spring.ch8.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.iakidovich.spring.ch8.entities.Album;
import maksim.iakidovich.spring.ch8.entities.Singer;
import maksim.iakidovich.spring.ch8.repositories.AlbumRepository;
import maksim.iakidovich.spring.ch8.services.AlbumService;

@Service
@Repository
@Transactional
public class AlbumServiceImpl implements AlbumService {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    private AlbumRepository albumRepository;
    
    @Transactional(readOnly = true)
    @Override
    public List<Album> findAll() {
        List<Album> resultList = entityManager.createQuery("select a from Album a", Album.class).getResultList();
        return resultList;
    }
    
    @Override
    public List<Album> findBySinger(Singer singer) {
        return albumRepository.findBySinger(singer);
    }
    
    @Override
    public List<Album> findByTitle(String title) {
        return albumRepository.findByTitle(title);
    }
}
