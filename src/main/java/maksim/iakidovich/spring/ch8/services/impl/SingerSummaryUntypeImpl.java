package maksim.iakidovich.spring.ch8.services.impl;

import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.iakidovich.spring.ch8.entities.SingerSummary;

@Service
@Repository
@Transactional
public class SingerSummaryUntypeImpl {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    public void displayAllSingerSummary() {
        List resultList = entityManager.createQuery("select s.firstName, s.lastName, a.title"
                                                    + " from Singer s left join s.albums a"
                                                    + " where a.releaseDate=(select max(a2.releaseDate) from Album a2"
                                                    + " where a2.singer.id = s.id)").getResultList();
        Iterator iterator = resultList.iterator();
        while (iterator.hasNext()) {
            Object[] next = (Object[]) iterator.next();
            System.out.println(next[0] + " " + next[1] + " " + next[2]);
        }
    }
    
    public List<SingerSummary> getAllSingerSummary() {
        List<SingerSummary> singerSummaries =
                entityManager.createQuery("select new maksim.iakidovich.spring.ch8.entities.SingerSummary(s.firstName, s.lastName, a.title)"
                                          + " from Singer s left join s.albums a"
                                          + " where a.releaseDate=(select max(a2.releaseDate) from Album a2"
                                          + " where a2.singer.id = s.id)", SingerSummary.class).getResultList();
        return singerSummaries;
    }
    
}
