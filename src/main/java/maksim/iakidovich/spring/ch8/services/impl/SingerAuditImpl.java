package maksim.iakidovich.spring.ch8.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.iakidovich.spring.ch8.entities.SingerAudit;
import maksim.iakidovich.spring.ch8.repositories.SingerAuditRepository;
import maksim.iakidovich.spring.ch8.services.SingerAuditService;

@Service
@Transactional
public class SingerAuditImpl implements SingerAuditService {
    
    @Autowired
    private SingerAuditRepository singerAuditRepository;
    
    @Override
    public List<SingerAudit> findAll() {
        return singerAuditRepository.findAll();
    }
    
    @Override
    public SingerAudit findById(Long id) {
        return singerAuditRepository.findById(id).get();
    }
    
    @Override
    public SingerAudit save(SingerAudit singerAudit) {
        return singerAuditRepository.save(singerAudit);
    }
}
