package maksim.iakidovich.spring.ch8.services;

import java.util.List;

import maksim.iakidovich.spring.ch8.entities.SingerAudit;

public interface SingerAuditService {
    List<SingerAudit> findAll();
    SingerAudit findById(Long id);
    SingerAudit save(SingerAudit singerAudit);
    
}
