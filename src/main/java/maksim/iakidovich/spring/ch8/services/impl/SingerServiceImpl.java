package maksim.iakidovich.spring.ch8.services.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import maksim.iakidovich.spring.ch8.entities.Singer;
import maksim.iakidovich.spring.ch8.entities.Singer_;
import maksim.iakidovich.spring.ch8.repositories.SingerRepository;
import maksim.iakidovich.spring.ch8.services.SingerService;

@Slf4j
@Service("jpaSingerService")
@Repository
@Transactional
public class SingerServiceImpl implements SingerService {
    private final static String ALL_SINGER_NATIVE_QUERY = "select id, first_name, last_name, birth_date, version from singer";
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @Autowired
    private SingerRepository singerRepository;
    
    @Transactional(readOnly = true)
    @Override
    public List<Singer> findAll() {
        return singerRepository.findAll();
    }
    
    @Override
    public List<Singer> findByFirstName(String firstName) {
        return singerRepository.findByFirstName(firstName);
    }
    
    @Override
    public List<Singer> findByFirstNameAndLastName(String firstName, String lastName) {
        return singerRepository.findByFirstNameAndLastName(firstName, lastName);
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Singer> findAllWithAlbums() {
        TypedQuery<Singer> singersWithAlbums = entityManager.createNamedQuery(Singer.FIND_ALL_WITH_ALBUM, Singer.class);
        return singersWithAlbums.getResultList();
    }
    
    @Transactional(readOnly = true)
    @Override
    public Singer findById(Long id) {
        TypedQuery<Singer> singerById = entityManager.createNamedQuery(Singer.FIND_SINGER_BY_ID, Singer.class);
        singerById.setParameter("id", id);
        return singerById.getSingleResult();
    }
    
    @Transactional(readOnly = true)
    @Override
    public List<Singer> findAllByNativeQuery() {
        @SuppressWarnings("unchecked")
        List<Singer> resultList =
                entityManager.createNativeQuery(ALL_SINGER_NATIVE_QUERY, "singerResult").getResultList();
        return resultList;
    }
    
    @Override
    public List<Singer> findByCriteriaQuery(String firstName, String lastName) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Singer> query = criteriaBuilder.createQuery(Singer.class);
        Root<Singer> from = query.from(Singer.class);
        from.fetch(Singer_.albums, JoinType.LEFT);
        from.fetch(Singer_.instruments, JoinType.LEFT);
        query.select(from).distinct(true);
        Predicate conjunction = criteriaBuilder.conjunction();
        if (firstName != null) {
            Predicate p = criteriaBuilder.equal(from.get(Singer_.firstName), firstName);
            conjunction = criteriaBuilder.and(conjunction, p);
        }
        query.where(conjunction);
        return entityManager.createQuery(query).getResultList();
    }
    
    @Override
    public Singer save(Singer singer) {
        if (singer.getId() == null) {
            log.info("======Inserting new Singer====== {}", singer);
            entityManager.persist(singer);
        } else  {
            log.info("======Update existing Singer==== {}", singer);
            entityManager.merge(singer);
        }
        log.info("====Singer saved with id {}======", singer.getId());
        return singer;
    }
    
    @Override
    public void delete(Singer singer) {
        Singer merge = entityManager.merge(singer);
        entityManager.remove(singer);
    }
}


