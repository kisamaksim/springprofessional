package maksim.iakidovich.spring.ch8.services;

import java.util.List;

import maksim.iakidovich.spring.ch8.entities.Album;
import maksim.iakidovich.spring.ch8.entities.Singer;

public interface AlbumService {
    List<Album> findAll();
    List<Album> findBySinger(Singer singer);
    List<Album> findByTitle(String title);
    
}
