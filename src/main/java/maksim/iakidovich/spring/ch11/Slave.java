package maksim.iakidovich.spring.ch11;

import java.util.concurrent.TimeUnit;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Slave {
    
    @Scheduled(fixedRate = 1000)
    public void doSomething() throws InterruptedException {
        System.out.println("I do something " + Thread.currentThread().getName());
        TimeUnit.SECONDS.sleep(4);
        System.out.println("I am done " + Thread.currentThread().getName());
    }
    
    @Async
    public void doSomethingTwo() throws InterruptedException {
        System.out.println("I do something " + Thread.currentThread().getName());
        TimeUnit.SECONDS.sleep(4);
        System.out.println("I am done " + Thread.currentThread().getName());
    }
}
