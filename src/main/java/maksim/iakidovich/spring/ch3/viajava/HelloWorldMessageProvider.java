package maksim.iakidovich.spring.ch3.viajava;

import org.springframework.stereotype.Component;

import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;

@Component("javaprovider")
public class HelloWorldMessageProvider implements MessageProvider {
    @Override
    public void getMessage() {
        System.out.println("Hello World!");
    }
}
