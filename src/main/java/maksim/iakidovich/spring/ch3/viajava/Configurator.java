package maksim.iakidovich.spring.ch3.viajava;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan(basePackages = "maksim.iakidovich.spring.ch3.viajava")
@Configuration
public class Configurator {
}
