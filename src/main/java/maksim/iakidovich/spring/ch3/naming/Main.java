package maksim.iakidovich.spring.ch3.naming;

import java.util.Arrays;
import java.util.Map;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext cont = new GenericXmlApplicationContext("app-conf-naming.xml");
        Map<String, String> beansOfType = cont.getBeansOfType(String.class);
        System.out.println(beansOfType);
        String[] strings = cont.getAliases("one");
        System.out.println(Arrays.toString(strings));
    }
}
