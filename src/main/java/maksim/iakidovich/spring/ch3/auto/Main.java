package maksim.iakidovich.spring.ch3.auto;

import java.util.Arrays;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        Bar bar = context.getBean("bar", Bar.class);
        System.out.println(bar);
    
//        GenericXmlApplicationContext context = new GenericXmlApplicationContext("app-conf-auto.xml");
//        Bar bean = context.getBean(Bar.class);
//        System.out.println(bean);
//        Foo foo = (Foo) context.getBean(Foo.class);
//        System.out.println(Arrays.toString(context.getBeanNamesForType(Foo.class)));
//        System.out.println(foo);
    }
}
