package maksim.iakidovich.spring.ch3.auto;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("maksim.iakidovich.spring.ch3.auto")
public class Config {
}
