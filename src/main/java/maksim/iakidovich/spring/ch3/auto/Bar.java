package maksim.iakidovich.spring.ch3.auto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
//@Setter
//@AllArgsConstructor
@Component
public class Bar {
    
    @Autowired
    private Foo fooOne;
    
//    public void setFoo(Foo foo) {
//        this.foo = foo;
//    }
}
