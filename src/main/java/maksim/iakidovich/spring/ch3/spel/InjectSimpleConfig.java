package maksim.iakidovich.spring.ch3.spel;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Component("injectsimple")
public class InjectSimpleConfig {
    @Value("#{conf.name}")
    private String name;
    @Value("#{conf.age}")
    private int age;
    @Value("#{conf.height}")
    private float height;
    @Value("#{conf.programmer}")
    private boolean programmer;
    @Value("#{conf.ageInSeconds}")
    private long ageInSeconds;
}
