package maksim.iakidovich.spring.ch3.spel;

import org.springframework.stereotype.Component;

import lombok.Getter;

@Getter
@Component("conf")
public class InjectSimple {
    private String name = "Maksim";
    private int age = 20;
    private float height = 75.1f;
    private boolean programmer = true;
    private long ageInSeconds = 123456;
}
