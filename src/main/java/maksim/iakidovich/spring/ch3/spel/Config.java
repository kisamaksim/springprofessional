package maksim.iakidovich.spring.ch3.spel;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.jmx.export.annotation.ManagedResource;

@ComponentScan("maksim.iakidovich.spring.ch3.spel")
public class Config {
    
    private List<String> list = new ArrayList<>();
}
