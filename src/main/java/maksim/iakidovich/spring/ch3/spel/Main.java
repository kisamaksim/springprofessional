package maksim.iakidovich.spring.ch3.spel;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
//        GenericXmlApplicationContext genericXmlApplicationContext =
//                new GenericXmlApplicationContext("app-config-spel.xml");
//        InjectSimpleConfig simpleinject = genericXmlApplicationContext.getBean("simpleconf", InjectSimpleConfig.class);
//        System.out.println(simpleinject);
    
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        InjectSimpleConfig injectsimple = context.getBean("injectsimple", InjectSimpleConfig.class);
        System.out.println(injectsimple);
    }
}
