package maksim.iakidovich.spring.ch3.injectviasearch;

import lombok.Setter;


public class StandartLookUpDemoBean implements DemoBean {
    @Setter
    private Singer singer;
    
    @Override
    public Singer getMySinger() {
        return singer;
    }
    
    @Override
    public void doSmt() {
        singer.sing();
    }
}
