package maksim.iakidovich.spring.ch3.injectviasearch;

public abstract class AbstractLookupDemoBean implements DemoBean {
    
    @Override
    abstract public Singer getMySinger();
    
    @Override
    public void doSmt() {
        getMySinger().sing();
    }
}
