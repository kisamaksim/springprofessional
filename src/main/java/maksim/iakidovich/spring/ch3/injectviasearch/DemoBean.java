package maksim.iakidovich.spring.ch3.injectviasearch;

public interface DemoBean {
    Singer getMySinger();
    void doSmt();
}
