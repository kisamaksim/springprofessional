package maksim.iakidovich.spring.ch3.injectviasearch;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext cont = new GenericXmlApplicationContext("app-config-lookup.xml");
        DemoBean search = cont.getBean("search", DemoBean.class);
        search.doSmt();
        Singer mySinger = search.getMySinger();
        Singer mySinger1 = search.getMySinger();
        Singer mySinger2 = search.getMySinger();
        DemoBean standart = cont.getBean("standart", DemoBean.class);
        Singer mySinger3 = standart.getMySinger();
        System.out.println();
    }
}
