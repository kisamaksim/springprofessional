package maksim.iakidovich.spring.ch3.collections;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.ToString;

@Component("injectsimple")
@Getter
@ToString
public class InjectSimple {
    
//    @Resource(name = "list")
    private List<String> list;
    
    @Autowired
    public void setList(List<String> test) {
        this.list = test;
    }
}
