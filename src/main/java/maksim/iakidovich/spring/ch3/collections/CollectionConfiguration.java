package maksim.iakidovich.spring.ch3.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("maksim.iakidovich.spring.ch3.collections")
public class CollectionConfiguration {
    
    @Bean("test")
    public List<String> getListTwo() {
        List<String> list = new ArrayList<>();
        Collections.addAll(list, "ONE", "TWO", "TREE");
        return list;
    }
    
    @Bean("list")
    public List<String> getList() {
        List<String> list = new ArrayList<>();
        Collections.addAll(list, "one", "two", "tree");
        return list;
    }
}
