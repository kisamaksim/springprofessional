package maksim.iakidovich.spring.ch3.collections;

import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(CollectionConfiguration.class);
        InjectSimple injectsimple = context.getBean("injectsimple", InjectSimple.class);
        System.out.println(injectsimple);
        List<String> list1 = injectsimple.getList();
        list1.add("maksim");
        System.out.println(injectsimple);
//        @SuppressWarnings("unchecked")
//        List<String> list = (List<String>) context.getBean("list");
//        System.out.println(list);
    }
}
