package maksim.iakidovich.spring.ch3.collections;

import java.util.List;

import lombok.Setter;

@Setter
public class InjectCollections {
    private List<Integer> integers;
}
