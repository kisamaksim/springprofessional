package maksim.iakidovich.spring.ch3.simpleconfig;

import org.springframework.stereotype.Component;

import jdk.nashorn.internal.ir.annotations.Ignore;
import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;

@Component("asap")
@Ignore
public class AsapMessageProvider implements MessageProvider {
    @Override
    public void getMessage() {
        System.out.println("ASAP");
    }
}
