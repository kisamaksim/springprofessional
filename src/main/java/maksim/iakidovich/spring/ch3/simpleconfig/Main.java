package maksim.iakidovich.spring.ch3.simpleconfig;

import org.springframework.context.support.GenericXmlApplicationContext;

import maksim.iakidovich.spring.ch2.interfaces.MessageRenderer;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext genericXmlApplicationContext =
                new GenericXmlApplicationContext("app-context-conf.xml");
        MessageRenderer renderer = genericXmlApplicationContext.getBean("renderer", MessageRenderer.class);
        renderer.render();
//        MessageRenderer renderer2 = genericXmlApplicationContext.getBean("renderer2", MessageRenderer.class);
//        renderer2.render();
    }
}
