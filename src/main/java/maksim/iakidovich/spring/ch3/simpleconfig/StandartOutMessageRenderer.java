package maksim.iakidovich.spring.ch3.simpleconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;
import maksim.iakidovich.spring.ch2.interfaces.MessageRenderer;

@Service("renderer")
public class StandartOutMessageRenderer implements MessageRenderer {
    private MessageProvider mp;
    
    @Override
    public void render() {
        mp.getMessage();
    }
    
    @Autowired
    @Override
    public void setMessageProvider(@Qualifier("provider") MessageProvider mp) {
        this.mp = mp;
    }
    
    @Override
    public MessageProvider getMessageProvider() {
        return mp;
    }
}
