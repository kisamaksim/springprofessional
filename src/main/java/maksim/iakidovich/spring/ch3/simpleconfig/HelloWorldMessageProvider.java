package maksim.iakidovich.spring.ch3.simpleconfig;

import org.springframework.stereotype.Component;

import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;

@Component("provider")
public class HelloWorldMessageProvider implements MessageProvider {
    @Override
    public void getMessage() {
        System.out.println("Hello World!");
    }
}
