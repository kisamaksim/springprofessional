package maksim.iakidovich.spring.ch9.programtransac.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("maksim.iakidovich.spring.ch9")
public class ServiceConfig {

}
