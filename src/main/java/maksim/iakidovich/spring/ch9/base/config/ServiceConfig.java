package maksim.iakidovich.spring.ch9.base.config;

import javax.persistence.EntityManagerFactory;

import org.hibernate.jpa.spi.HibernateEntityManagerImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan("maksim.iakidovich.spring.ch9")
public class ServiceConfig {
    
    @Autowired
    EntityManagerFactory entityManagerFactory;
    
    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory);
    }
}
