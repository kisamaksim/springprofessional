package maksim.iakidovich.spring.ch9.base;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import maksim.iakidovich.spring.ch9.base.config.JpaConfig;
import maksim.iakidovich.spring.ch9.base.config.ServiceConfig;
import maksim.iakidovich.spring.ch9.base.entities.Singer;
import maksim.iakidovich.spring.ch9.base.services.SingerService;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServiceConfig.class, JpaConfig.class);
        SingerService singerService = context.getBean(SingerService.class);
    
        Singer singer = new Singer();
        singer.setFirstName("Maksim");
        singer.setLastName("Ia");
        singer.setBirthDate(new Date(new GregorianCalendar(1996, 6, 20).getTime().getTime()));
        
        singerService.save(singer);
        
        System.out.println(singerService.findAll());
    }
}
