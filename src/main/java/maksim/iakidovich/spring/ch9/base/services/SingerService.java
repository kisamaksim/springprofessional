package maksim.iakidovich.spring.ch9.base.services;

import java.util.List;

import maksim.iakidovich.spring.ch9.base.entities.Singer;

public interface SingerService {
    List<Singer> findAll();
    
    Singer findById(Long id);
    
    Singer save(Singer singer);
    
    long countAll();
}
