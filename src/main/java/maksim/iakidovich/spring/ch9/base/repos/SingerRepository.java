package maksim.iakidovich.spring.ch9.base.repos;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import maksim.iakidovich.spring.ch9.base.entities.Singer;

public interface SingerRepository extends CrudRepository<Singer, Long> {
    
    @Query("select count(s) from Singer s")
    Long countAllSingers();
    
    @Override
    List<Singer> findAll();
}
