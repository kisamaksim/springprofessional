package maksim.iakidovich.spring.ch9.base.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.iakidovich.spring.ch9.base.entities.Singer;
import maksim.iakidovich.spring.ch9.base.repos.SingerRepository;
import maksim.iakidovich.spring.ch9.base.services.SingerService;

@Service("singerService")
@Transactional
public class SingerServiceImpl implements SingerService {
    
    private SingerRepository singerRepository;
    
    @Autowired
    public SingerServiceImpl(SingerRepository singerRepository) {
        this.singerRepository = singerRepository;
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Singer> findAll() {
        return singerRepository.findAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Singer findById(Long id) {
        return singerRepository.findById(id).get();
    }
    
    @Override
    public Singer save(Singer singer) {
        return singerRepository.save(singer);
    }
    
    @Override
    public long countAll() {
        return 0;
    }
}
