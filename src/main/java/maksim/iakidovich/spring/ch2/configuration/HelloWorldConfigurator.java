package maksim.iakidovich.spring.ch2.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import maksim.iakidovich.spring.ch2.impl.HelloWorldMessageProvider;
import maksim.iakidovich.spring.ch2.impl.StandartOutMessageRenderer;
import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;
import maksim.iakidovich.spring.ch2.interfaces.MessageRenderer;

@Configuration
public class HelloWorldConfigurator {
    
    @Bean
    public MessageProvider provider() {
        return new HelloWorldMessageProvider();
    }
    
    @Bean
    public MessageRenderer renderer() {
        MessageRenderer renderer = new StandartOutMessageRenderer();
        renderer.setMessageProvider(provider());
        return renderer;
    }
}
