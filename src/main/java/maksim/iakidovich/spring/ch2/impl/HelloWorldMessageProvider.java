package maksim.iakidovich.spring.ch2.impl;

import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;

public class HelloWorldMessageProvider implements MessageProvider {
    @Override
    public void getMessage() {
        System.out.println("Hello World!");
    }
}
