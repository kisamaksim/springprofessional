package maksim.iakidovich.spring.ch2.impl;

import maksim.iakidovich.spring.ch2.interfaces.MessageProvider;
import maksim.iakidovich.spring.ch2.interfaces.MessageRenderer;

public class StandartOutMessageRenderer implements MessageRenderer {
    private MessageProvider mp;
    
    @Override
    public void render() {
        mp.getMessage();
    }
    
    @Override
    public void setMessageProvider(MessageProvider mp) {
        this.mp = mp;
    }
    
    @Override
    public MessageProvider getMessageProvider() {
        return mp;
    }
}
