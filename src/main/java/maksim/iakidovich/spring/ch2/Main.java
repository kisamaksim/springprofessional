package maksim.iakidovich.spring.ch2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import maksim.iakidovich.spring.ch2.configuration.HelloWorldConfigurator;
import maksim.iakidovich.spring.ch2.interfaces.MessageRenderer;

public class Main {
    public static void main(String[] args) {
        ApplicationContext ac = new AnnotationConfigApplicationContext(HelloWorldConfigurator.class);
        MessageRenderer renderer = ac.getBean("renderer", MessageRenderer.class);
        renderer.render();
    }
}
