package maksim.iakidovich.spring.ch2.interfaces;

public interface MessageProvider {
    void getMessage();
}
