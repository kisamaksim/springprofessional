package maksim.iakidovich.spring.ch2.interfaces;

public interface MessageRenderer {
    void render();
    void setMessageProvider(MessageProvider mp);
    MessageProvider getMessageProvider();
}
