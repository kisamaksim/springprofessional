package maksim.iakidovich.spring.ch10;

import java.text.ParseException;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.format.Formatter;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.stereotype.Component;

@Component
public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {
    
    private final String DATE_TIME_PATTERN = "yyyy-MM-dd";
    private DateTimeFormatter dateFormat;
    
    @PostConstruct
    public void init() {
        dateFormat = DateTimeFormat.forPattern(DATE_TIME_PATTERN);
    }
    
    public Formatter<DateTime> getDateTimeFormatter() {
        return new Formatter<DateTime>() {
            @Override
            public DateTime parse(String text, Locale locale) throws ParseException {
                return dateFormat.parseDateTime(text);
            }
    
            @Override
            public String print(DateTime object, Locale locale) {
                return object.toString();
            }
        };
    }
}
