package maksim.iakidovich.spring.ch10;

import java.net.URL;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.joda.time.DateTime;
import org.springframework.stereotype.Component;

import lombok.Data;

@Data
@Component
public class Human {
    @NotNull
    @Size(min = 2, max = 10)
    private String firstName;
    private String lastName;
    private DateTime birthDate;
    private URL personalSite;
    @Size(max = 100)
    private int age;
    
    
    public static void main(String[] args) {
        Human human = new Human();
        human.setBirthDate(DateTime.parse("1996-06-20"));
        
        System.out.println(human);
    }
}

