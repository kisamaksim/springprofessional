package maksim.iakidovich.spring.ch10;

import java.text.ParseException;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
@ComponentScan("maksim.iakidovich.spring.ch10")
@PropertySource("classpath:ch10/application.properties")
public class AppConfig {
    
    @Autowired
    private ApplicationConversionServiceFactoryBean formatter;
    
//    @Bean
//    public Human human(@Value("${firstName}") String firsName,
//                       @Value("${lastName}") String lastName,
//                       @Value("${birthDate}") String birthDate) throws ParseException {
//        Human human = new Human();
//        human.setFirstName(firsName);
//        human.setLastName(lastName);
//        human.setBirthDate(formatter.getDateTimeFormatter().parse(birthDate, Locale.ENGLISH));
//        return human;
//    }
    
//    @Bean
//    public StringToDateTimeConverter stringToDateTimeConverter() {
//        return new StringToDateTimeConverter();
//    }
//
//    @Bean
//    public ConversionServiceFactoryBean conversionService() {
//        ConversionServiceFactoryBean conversionServiceFactoryBean = new ConversionServiceFactoryBean();
//        Set<Converter<?, ?>> converters = new HashSet<>();
//        converters.add(stringToDateTimeConverter());
//        conversionServiceFactoryBean.setConverters(converters);
//        conversionServiceFactoryBean.afterPropertiesSet();
//        return conversionServiceFactoryBean;
//    }
    
    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean() {
        return new LocalValidatorFactoryBean();
    }
    
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        Human human = context.getBean("human", Human.class);
        System.out.println(human);
        
    }
}
