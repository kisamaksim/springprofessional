package maksim.iakidovich.spring.ch10;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToDateTimeConverter implements Converter<String, DateTime> {
    
    private final String DATE_TIME_PATTERN = "yyyy-MM-dd";
    private DateTimeFormatter dateFormat;
    
    @Override
    public DateTime convert(String source) {
        return dateFormat.parseDateTime(source);
    }
    
    @PostConstruct
    public void init() {
        dateFormat = DateTimeFormat.forPattern(DATE_TIME_PATTERN);
    }
}
