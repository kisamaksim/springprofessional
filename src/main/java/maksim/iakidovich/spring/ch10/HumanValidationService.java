package maksim.iakidovich.spring.ch10;

import java.util.Set;

import javax.validation.ConstraintViolation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.validation.Validator;

@Service
public class HumanValidationService {
    
    @Autowired
    private Validator validator;
    
    public Set<ConstraintViolation<Human>> validateHuman(Human human) {
        return validator.validate(human);
    }
}
