package maksim.iakidovich.spring.ch7.sub1.dao.interfaces;

import java.util.List;

import maksim.iakidovich.spring.ch7.sub1.entities.Singer;

public interface SingerDao {
    List<Singer> findAll();
    List<Singer> findAllWithAlbums();
    Singer findById(Long id);
    Singer save(Singer contact);
    void delete(Singer contact);
}
