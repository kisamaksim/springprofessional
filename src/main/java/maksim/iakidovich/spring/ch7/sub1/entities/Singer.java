package maksim.iakidovich.spring.ch7.sub1.entities;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "Singer")
@Table(name = "singer")
@Setter
@Getter
@ToString
@NamedQueries({@NamedQuery(name="Singer.findAllWithAlbum",
                           query="select distinct s from Singer s "
                                   + "left join fetch s.albums a "
                                   + "left join fetch s.instruments i"),
              @NamedQuery(name = "Singer.findById",
                          query = "select distinct s from Singer s "
                                  + "left join fetch s.albums a "
                                  + "left join fetch s.instruments i "
                                  + "where s.id = :id")})
public class Singer implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "FIRST_NAME")
    private String firstName;
    
    @Column(name = "LAST_NAME")
    private String lastName;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "BIRTH_DATE")
    private Date birthDate;
    
    private Integer version;
    
    @Version
    @Column(name = "VERSION")
    public Integer getVersion() {
        return version;
    }
    
    @OneToMany(mappedBy = "singer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Album> albums = new ArrayList<>();
    
    @ManyToMany()
    @JoinTable(name = "SINGER_INSTRUMENT",
               joinColumns = @JoinColumn(name = "SINGER_ID"),
               inverseJoinColumns = @JoinColumn(name = "INSTRUMENT_ID"))
    private Set<Instrument> instruments = new HashSet<>();
    
    public boolean addAlbum(Album album) {
        album.setSinger(this);
        return getAlbums().add(album);
    }
}
