package maksim.iakidovich.spring.ch7.sub1;

import java.sql.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import maksim.iakidovich.spring.ch7.sub1.dao.interfaces.SingerDao;
import maksim.iakidovich.spring.ch7.sub1.entities.Singer;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HibernateConfig.class);
        SingerDao singerDao = context.getBean(SingerDao.class);
        Singer test = new Singer();
        test.setFirstName("test");
        test.setLastName("testov");
        test.setBirthDate(new Date(new GregorianCalendar(1961, 7, 18).getTime().getTime()));
        Singer save = singerDao.save(test);
        System.out.println(save);
    }
}
