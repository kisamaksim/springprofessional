package maksim.iakidovich.spring.ch7.sub1.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import maksim.iakidovich.spring.ch7.sub1.dao.interfaces.SingerDao;
import maksim.iakidovich.spring.ch7.sub1.entities.Singer;

@Slf4j
@Data
@Repository("singerDao")
@Transactional
public class SingerDaoImpl implements SingerDao {
    
    @Resource(name = "sessionFactory")
    private SessionFactory sessionFactory;
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Singer> findAll() {
        return sessionFactory.getCurrentSession().createQuery("from Singer s").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Singer> findAllWithAlbums() {
        return sessionFactory.getCurrentSession().getNamedQuery("Singer.findAllWithAlbum").list();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Singer findById(Long id) {
        return (Singer) sessionFactory.getCurrentSession().getNamedQuery("Singer.findById")
                                                          .setParameter("id", id).uniqueResult();
    }
    
    @Override
    public Singer save(Singer contact) {
        sessionFactory.getCurrentSession().saveOrUpdate("Singer", contact);
        System.out.println("blabla new added: " + contact.getId());
        return contact;
    }
    
    @Override
    public void delete(Singer contact) {
    
    }
    
}
