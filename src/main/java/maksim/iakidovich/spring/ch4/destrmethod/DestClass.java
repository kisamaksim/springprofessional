package maksim.iakidovich.spring.ch4.destrmethod;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

import lombok.ToString;

@Component
@ToString
public class DestClass {
    
    private String s;
    
    @PreDestroy
    private void destroy() {
        System.out.println("destroy");
    }
}
