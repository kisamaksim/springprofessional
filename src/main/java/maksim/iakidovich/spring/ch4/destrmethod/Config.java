package maksim.iakidovich.spring.ch4.destrmethod;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
@ComponentScan("maksim.iakidovich.spring.ch4.destrmethod")
public class Config {
}
