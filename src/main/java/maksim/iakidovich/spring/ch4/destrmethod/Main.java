package maksim.iakidovich.spring.ch4.destrmethod;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        DestClass bean = context.getBean(DestClass.class);
        
    }
}
