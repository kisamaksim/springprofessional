package maksim.iakidovich.spring.ch4.javaconfig;

import lombok.Setter;

@Setter
public class First {
    private Second second;
    
    public First() {
        System.out.println("First() init");
    }
    
    public void f() {
        System.out.println(second.getString());
    }
    
}
