package maksim.iakidovich.spring.ch4.javaconfig;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigurationApp.class);
        First bean = context.getBean(First.class);
        bean.f();
    }
}
