package maksim.iakidovich.spring.ch4.javaconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@Lazy
@PropertySource(value = {"classpath:ch4/message.properties","classpath:ch4/labels_en.properties"})
public class ConfigurationApp {
    
    @Autowired
    Environment environment;
    
    @Bean
    public Second getSecond() {
        return new Second(environment.getProperty("msg"));
    }
    
    @Bean
    public First getFirst() {
        First first = new First();
        first.setSecond(getSecond());
        return first;
    }
    
}
