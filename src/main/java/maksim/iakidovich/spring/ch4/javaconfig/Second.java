package maksim.iakidovich.spring.ch4.javaconfig;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Second {
    private String string;
}
