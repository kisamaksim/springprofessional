package maksim.iakidovich.spring.ch4.eventlisten;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("maksim.iakidovich.spring.ch4.eventlisten")
public class Config {
}
