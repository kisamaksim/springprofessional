package maksim.iakidovich.spring.ch4.eventlisten;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        GenericXmlApplicationContext context = new GenericXmlApplicationContext("ch4/app-conf-event.xml");
        Publisher bean = context.getBean(Publisher.class);
        bean.publish("hiii"); //два лисенера, два перехвата
    }
}
