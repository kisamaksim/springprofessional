package maksim.iakidovich.spring.ch4.miniweb;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hell")
public class Hell {
    
    @GetMapping("/hi")
    public ResponseEntity<String> test() {
        return new ResponseEntity<>("TI PIDAR)))", HttpStatus.OK);
    }
    
}
