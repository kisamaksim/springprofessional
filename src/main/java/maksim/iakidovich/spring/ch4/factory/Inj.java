package maksim.iakidovich.spring.ch4.factory;

import java.security.MessageDigest;
import java.util.Arrays;

import lombok.AllArgsConstructor;
import lombok.Setter;

@Setter
//@AllArgsConstructor
public class Inj {
    
    private MessageDigest m1;
    private MessageDigest m2;
    
    public void print(String message) {
        byte[] bytes = message.getBytes();
        System.out.println(m1.getAlgorithm() + " -> " + Arrays.toString(m1.digest(bytes)));
        System.out.println(m2.getAlgorithm() + " -> " + Arrays.toString(m2.digest(bytes)));
    }
}
