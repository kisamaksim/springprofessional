package maksim.iakidovich.spring.ch4.factory;

import java.security.MessageDigest;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

import lombok.Setter;

@Setter
public class MessageFactory implements FactoryBean<MessageDigest>, InitializingBean {
    
    private MessageDigest messageDigest;
    private String algorithm = "MD5";
    
    @Override
    public MessageDigest getObject() throws Exception {
        return messageDigest;
    }
    
    @Override
    public Class<?> getObjectType() {
        return MessageDigest.class;
    }
    
    @Override
    public boolean isSingleton() {
        return false;
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        messageDigest = MessageDigest.getInstance(algorithm);
    }
}
