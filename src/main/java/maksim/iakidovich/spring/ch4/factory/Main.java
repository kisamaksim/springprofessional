package maksim.iakidovich.spring.ch4.factory;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext("ch4/app-conf-fac.xml");
        Inj bean = context.getBean(Inj.class);
        bean.print("maksim");
    }
}
