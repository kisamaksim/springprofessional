package maksim.iakidovich.spring.ch4.environment;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@ComponentScan("maksim.iakidovich.spring.ch4.environment")
@PropertySource("classpath:ch4/message.properties")
public class Config {
}
