package maksim.iakidovich.spring.ch4.environment;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@Lazy
public class Env {
    
    @Value("${user.home}")
    private String userHome;
    
    public Env() {
        System.out.println("ENV CONTRUCTOR");
    }
}
