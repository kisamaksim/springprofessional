package maksim.iakidovich.spring.ch4.environment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.Setter;

@Component
@Setter
public class EnvHolder {
    
    @Autowired
    private Env env;
}
