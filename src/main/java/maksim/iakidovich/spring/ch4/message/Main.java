package maksim.iakidovich.spring.ch4.message;

import java.util.Locale;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext("ch4/app-conf-mes.xml");
        Locale english = Locale.ENGLISH;
        System.out.println(context.getMessage("msg", null, english));
        System.out.println(context.getMessage("msg1", new Object[] {"vitaly"}, english));
        
        System.out.println(context.getMessage(() -> new String[] {"msg", "msg1"}, english));
    }
}
