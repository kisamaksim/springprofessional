package maksim.iakidovich.spring.ch4.groove;

import lombok.Data;

@Data
public class SInger {
    private String name;
    private int age;
}
