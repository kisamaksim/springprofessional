package maksim.iakidovich.spring.ch4.initmethod;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

import lombok.ToString;

@ToString
public class InitClass {
    
    private String name;
    
    private void init() {
        System.out.println("init() method");
        if (name == null) {
            name = "maks";
        }
    }
    
    @PostConstruct
    private void init2() {
        System.out.println(name + " init2()");
    }
    
}
