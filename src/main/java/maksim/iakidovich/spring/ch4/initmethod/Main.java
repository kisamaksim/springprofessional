package maksim.iakidovich.spring.ch4.initmethod;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConfigCl.class);
        context.registerShutdownHook();
        InitClass bean = context.getBean(InitClass.class);
        System.out.println(bean);
    }
}
