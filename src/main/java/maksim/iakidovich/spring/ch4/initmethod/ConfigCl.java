package maksim.iakidovich.spring.ch4.initmethod;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigCl {
    
    @Bean(initMethod = "init")
    public InitClass getInit() {
        return new InitClass();
    }
    
}
