package maksim.iakidovich.spring.ch12.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import maksim.iakidovich.spring.ch12.entities.Singer;
import maksim.iakidovich.spring.ch12.repos.SingerRepository;
import maksim.iakidovich.spring.ch12.services.SingerService;

@Slf4j
@RestController
@RequestMapping("/singer")
public class SingerController {
    
    private SingerService singerService;
    
    @Autowired
    public SingerController(SingerService singerService) {
        this.singerService = singerService;
    }
    
    @GetMapping("/all")
    @ResponseStatus(HttpStatus.OK)
    public List<Singer> getAllSingers() {
        return singerService.findAll();
    }
    
    @GetMapping("/{id}")
    public Singer findById(@PathVariable Long id) {
        return singerService.findById(id);
    }
    
    @PostMapping("/")
    public void addSinger(@RequestBody Singer singer) {
        singerService.save(singer);
    }
}
