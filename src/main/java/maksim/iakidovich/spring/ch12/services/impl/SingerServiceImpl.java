package maksim.iakidovich.spring.ch12.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import maksim.iakidovich.spring.ch12.entities.Singer;
import maksim.iakidovich.spring.ch12.repos.SingerRepository;
import maksim.iakidovich.spring.ch12.services.SingerService;

@Service
@Transactional
public class SingerServiceImpl implements SingerService {
    
    private SingerRepository singerRepository;
    
    @Autowired
    public SingerServiceImpl(SingerRepository singerRepository) {
        this.singerRepository = singerRepository;
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Singer> findAll() {
        return singerRepository.findAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<Singer> findByFirstName(String firstName) {
        return singerRepository.findByFirstName(firstName);
    }
    
    @Override
    @Transactional(readOnly = true)
    public Singer findById(Long id) {
        return singerRepository.findById(id).orElse(new Singer());
    }
    
    @Override
    public Singer save(Singer singer) {
        return singerRepository.save(singer);
    }
    
    @Override
    public void delete(Singer singer) {
        singerRepository.delete(singer);
    }
}
