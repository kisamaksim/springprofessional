package maksim.iakidovich.spring.ch12.rabbit;

import java.io.IOException;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages = "maksim.iakidovich.spring.ch12.rabbit")
public class RabbitApplication {
    
    public final static String queueName = "forecast";
    public final static String exchangeName = "weather";
    
    @Bean
    public CachingConnectionFactory connectionFactory() {
        return new CachingConnectionFactory("localhost");
    }
    
    @Bean
    public Queue forecast() {
        return new Queue(queueName, true);
    }
    
    @Bean
    public Binding dataBind(DirectExchange directExchange, Queue queue) {
        return BindingBuilder.bind(queue).to(directExchange).with(queueName);
    }
    
    @Bean
    public DirectExchange weather() {
        return new DirectExchange(exchangeName, true, false);
    }
    
    @Bean
    public SimpleMessageListenerContainer simpleMessageListenerContainer() {
        SimpleMessageListenerContainer simpleMessageListenerContainer = new SimpleMessageListenerContainer();
        simpleMessageListenerContainer.setConnectionFactory(connectionFactory());
        simpleMessageListenerContainer.setQueueNames(queueName);
        return simpleMessageListenerContainer;
    }
    
    public static void main(String[] args) throws IOException {
        ConfigurableApplicationContext run = SpringApplication.run(RabbitApplication.class, args);
        WeatherStation bean1 = run.getBean(WeatherStation.class);
        RabbitTemplate bean = run.getBean(RabbitTemplate.class);
        bean.convertAndSend(RabbitApplication.queueName, "FL");
        bean.convertAndSend(RabbitApplication.queueName, "MA");
        bean.convertAndSend(RabbitApplication.queueName, "HBK");
        
        System.in.read();
        run.close();
    }
}
