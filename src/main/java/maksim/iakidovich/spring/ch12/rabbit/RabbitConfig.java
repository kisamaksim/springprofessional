//package maksim.iakidovich.spring.ch12.rabbit;
//
//import java.io.IOException;
//
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.BindingBuilder;
//import org.springframework.amqp.core.DirectExchange;
//import org.springframework.amqp.core.Queue;
//import org.springframework.amqp.rabbit.annotation.EnableRabbit;
//import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
//import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
//import org.springframework.amqp.rabbit.core.RabbitAdmin;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//@ComponentScan("maksim.iakidovich.spring.ch12.rabbit")
//@EnableRabbit
//public class RabbitConfig {
//
//    @Bean
//    public CachingConnectionFactory connectionFactory() {
//        return new CachingConnectionFactory("localhost");
//    }
//
//    @Bean
//    public RabbitTemplate amqpTemplate() {
//        RabbitTemplate rabbitTemplate = new RabbitTemplate();
//        rabbitTemplate.setConnectionFactory(connectionFactory());
//        rabbitTemplate.setReplyTimeout(2000);
//        rabbitTemplate.setRoutingKey("forecast");
//        rabbitTemplate.setExchange("weather");
//        return rabbitTemplate;
//    }
//
//    @Bean
//    public Queue forecast() {
//        return new Queue("forecast", true);
//    }
//
//    @Bean
//    public Binding dataBind(DirectExchange directExchange, Queue queue) {
//        return BindingBuilder.bind(queue).to(directExchange).with("forecast");
//    }
//
//    @Bean
//    public RabbitAdmin rabbitAdmin() {
//        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory());
//        rabbitAdmin.declareQueue(forecast());
//        rabbitAdmin.declareBinding(dataBind(weather(), forecast()));
//        return rabbitAdmin;
//    }
//
//    @Bean
//    public DirectExchange weather() {
//        return new DirectExchange("weather", true, false);
//    }
//
//    @Bean
//    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory() {
//        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
//        factory.setConnectionFactory(connectionFactory());
//        factory.setMaxConcurrentConsumers(5);
//        return factory;
//    }
//
//    public static void main(String[] args) throws IOException {
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RabbitConfig.class);
//        RabbitTemplate rabbitTemplate = context.getBean(RabbitTemplate.class);
//        rabbitTemplate.convertAndSend("FL");
//        rabbitTemplate.convertAndSend("MA");
//        rabbitTemplate.convertAndSend("HBK");
//
//        System.in.read();
//        context.close();
//    }
//
//}
