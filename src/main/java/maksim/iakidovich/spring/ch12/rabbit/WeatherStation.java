package maksim.iakidovich.spring.ch12.rabbit;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class WeatherStation {
    
    @RabbitListener(queues = "forecast")
    public void getForecast(String stateCode) {
        if ("FL".equals(stateCode)) {
            System.out.println("HOT");
            return;
        } else if ("MA".equals(stateCode)) {
            System.out.println("COLD");
            return;
        }
        System.out.println("I don't care!");
    }
}
