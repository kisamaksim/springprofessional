package maksim.iakidovich.spring.ch12.repos;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import maksim.iakidovich.spring.ch12.entities.Singer;

public interface SingerRepository extends CrudRepository<Singer, Long> {
    @Override
    List<Singer> findAll();
    
    List<Singer> findByFirstName(String firstName);
}
