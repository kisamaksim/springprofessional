package maksim.iakidovich.spring.ch6.ex;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Album {
    private Long id;
    private Long singerId;
    private String title;
    private Date releaseDate;
}
