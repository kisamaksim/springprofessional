package maksim.iakidovich.spring.ch6.ex;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PlainSingerDao implements SingerDao {
    
    static {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            log.error("problem", e);
        }
    }
    
    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:mysql://localhost:3306/musicdb",
                                           "prospring5", "prospring5");
    }
    
    @Override
    public List<Singer> findAll() {
        List<Singer> singers = new ArrayList<>();
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from singer");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Singer singer = new Singer();
                singer.setId(resultSet.getLong("id"));
                singer.setFirstName(resultSet.getNString("first_name"));
                singer.setLastName(resultSet.getNString("last_name"));
                singer.setBirthDate(resultSet.getDate("birth_date"));
                singers.add(singer);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            log.error("ex", e);
        }
        return singers;
    }
    
    @Override
    public List<Singer> findByFirstName(String firstName) {
        return null;
    }
    
    @Override
    public String findLastNameById(Long id) {
        return null;
    }
    
    @Override
    public String findFirstNameById(Long id) {
        return null;
    }
    
    @Override
    public void insert(Singer singer) {
        try (Connection connection = getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into singer (FIRST_NAME, LAST_NAME, BIRTH_DATE) values (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, singer.getFirstName());
            preparedStatement.setString(2, singer.getLastName());
            preparedStatement.setDate(3, singer.getBirthDate());
            preparedStatement.execute();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                singer.setId(generatedKeys.getLong(1));
            }
        } catch (SQLException e) {
            log.error("smth with connection");
        }
    }
    
    @Override
    public void update(Singer singer) {
    
    }
    
    @Override
    public void delete(Singer singer) {
    
    }
    
    @Override
    public List<Singer> findAllWithDetail() {
        return null;
    }
    
    @Override
    public void insertWithDetail(Singer singer) {
    
    }
}
