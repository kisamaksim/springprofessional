package maksim.iakidovich.spring.ch6.ex;

import java.util.List;

import maksim.iakidovich.spring.ch6.ex.Singer;

public interface SingerDao {
    List<Singer> findAll();
    List<Singer> findByFirstName(String firstName);
    String findLastNameById(Long id);
    String findFirstNameById(Long id);
    void insert(Singer singer);
    void update(Singer singer);
    void delete(Singer singer);
    List<Singer> findAllWithDetail();
    void insertWithDetail(Singer singer);
}
