package maksim.iakidovich.spring.ch6.ex;

import java.util.List;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

public class Main {
    
    private static PlainSingerDao plainSingerDao = new PlainSingerDao();
    
    public static void main(String[] args) {
        List<Singer> all = plainSingerDao.findAll();
        System.out.println(all);
    }
}
