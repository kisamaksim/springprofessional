package maksim.iakidovich.spring.ch6.sub3;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.jdbc.core.JdbcTemplate;

import lombok.Data;
import maksim.iakidovich.spring.ch6.ex.Singer;

@Data
public class JdbcSingerDao implements SingerDao, InitializingBean {
    
    private JdbcTemplate jdbcTemplate;
    
    @Override
    public void afterPropertiesSet() {
        if (jdbcTemplate.getDataSource() == null) {
            throw new BeanCreationException("Must Set DataSource on SingerDao");
        }
    }
    
    @Override
    public List<Singer> findAll() {
        return null;
    }
    
    @Override
    public String findNameById(Long id) {
        return jdbcTemplate.queryForObject("select first_name || ' ' || last_name from singer where id=?",
                                           new Object[]{id}, String.class);
    }
    
    @Override
    public Long findIdByName(String firstName, String lastName) {
        return jdbcTemplate.queryForObject("select id from singer where first_name=? AND last_name=?",
                                  new Object[]{firstName, lastName}, Long.class);
    }
}
