package maksim.iakidovich.spring.ch6.sub3;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import lombok.extern.slf4j.Slf4j;
import maksim.iakidovich.spring.ch6.sub5point3.JdbcSingerDaoTwo;

@Configuration
@Slf4j
public class EmbeddedJdbcConfig {
    
    @Bean
    public DataSource dataSource() {
        try {
            EmbeddedDatabaseBuilder dbBuilder = new EmbeddedDatabaseBuilder();
            return  dbBuilder.setType(EmbeddedDatabaseType.H2)
                             .addScripts("classpath:ch6/schema.sql", "classpath:ch6/test-data.sql")
                             .build();
        } catch (Exception e) {
            log.error("Embedded datasource Bean cannot be created", e);
            return null;
        }
    }
    
    @Bean
    public JdbcTemplate jdbcTemplate () {
        return new JdbcTemplate(dataSource());
    }
    
    @Bean
    public SingerDao singerDao() {
        JdbcSingerDao dao = new JdbcSingerDao();
        dao.setJdbcTemplate(jdbcTemplate());
        return dao;
    }
    
    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }
    
    @Bean
    @Primary
    public SingerDao singerDaoTwo() {
        JdbcSingerDaoTwo jdbcSingerDaoTwo = new JdbcSingerDaoTwo();
        jdbcSingerDaoTwo.setJdbcTemplate(namedParameterJdbcTemplate());
        return jdbcSingerDaoTwo;
    }
}
