package maksim.iakidovich.spring.ch6.sub3;

import java.util.Collections;
import java.util.List;

import maksim.iakidovich.spring.ch6.ex.Singer;

public interface SingerDao {
    String findNameById(Long id);
    Long findIdByName(String firstName, String lastName);
    List<Singer> findAll();
    default List<Singer> findAllWithAlbums() {
        return Collections.emptyList();
    }
    default void update(Singer singer) {}
}
