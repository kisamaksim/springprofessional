package maksim.iakidovich.spring.ch6.sub3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(EmbeddedJdbcConfig.class);
        SingerDao singerDao = context.getBean("singerDao", SingerDao.class);
        System.out.println(singerDao.findNameById(1L));
        System.out.println(singerDao.findIdByName("John", "Mayer"));
    }
}
