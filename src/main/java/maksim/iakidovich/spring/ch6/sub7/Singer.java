package maksim.iakidovich.spring.ch6.sub7;

public interface Singer {
    String findNameById(Long id);
}
