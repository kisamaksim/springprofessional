package maksim.iakidovich.spring.ch6.sub7;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

@Configuration
@ComponentScan("maksim.iakidovich.spring.ch6.sub7")
@PropertySource("classpath:ch6/db.properties")
public class Conf {
    
    @Value("${driverClassName}")
    private String driverClassName;
    
    @Bean
    public BasicDataSource basicDataSource() {
        BasicDataSource basicDataSource = new BasicDataSource();
        basicDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        basicDataSource.setUrl("jdbc:mysql://localhost:3306/musicdb");
        basicDataSource.setUsername("prospring5");
        basicDataSource.setPassword("prospring5");
        return basicDataSource;
    }
    
    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(basicDataSource());
    }
}
