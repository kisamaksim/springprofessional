package maksim.iakidovich.spring.ch6.sub7;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import lombok.Setter;

@Repository
@Setter
public class RapSinger implements Singer {
    
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    @Override
    public String findNameById(Long id) {
        String sql = "select CONCAT_WS(\" \", first_name, last_name) from singer where id= :singerId";
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("singerId", id);
        return jdbcTemplate.queryForObject(sql, namedParameters, String.class);
    }
}
