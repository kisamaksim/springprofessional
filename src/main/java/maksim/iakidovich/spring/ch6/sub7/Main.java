package maksim.iakidovich.spring.ch6.sub7;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Conf.class);
        Singer bean = context.getBean(Singer.class);
        System.out.println(bean.findNameById(2L));
    }
}
