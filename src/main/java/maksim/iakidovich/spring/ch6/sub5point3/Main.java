package maksim.iakidovich.spring.ch6.sub5point3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import maksim.iakidovich.spring.ch6.ex.Singer;
import maksim.iakidovich.spring.ch6.sub3.EmbeddedJdbcConfig;
import maksim.iakidovich.spring.ch6.sub3.SingerDao;

public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(EmbeddedJdbcConfig.class);
        SingerDao singerDaoTwo = context.getBean(SingerDao.class);
        System.out.println(singerDaoTwo.findNameById(2L));
        System.out.println(singerDaoTwo.findAll());
        System.out.println(singerDaoTwo.findAllWithAlbums());
        Singer singer = new Singer();
        singer.setFirstName("Maksim");
        singer.setId(1L);
        singerDaoTwo.update(singer);
        System.out.println(singerDaoTwo.findAll());
    
    }
}
