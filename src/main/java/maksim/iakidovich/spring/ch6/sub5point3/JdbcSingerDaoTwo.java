package maksim.iakidovich.spring.ch6.sub5point3;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import lombok.Data;
import maksim.iakidovich.spring.ch6.ex.Album;
import maksim.iakidovich.spring.ch6.ex.Singer;
import maksim.iakidovich.spring.ch6.sub3.SingerDao;

@Data
public class JdbcSingerDaoTwo implements SingerDao {
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    @Override
    public String findNameById(Long id) {
        String sql = "select first_name || ' ' || last_name from singer where id= :singerId";
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("singerId", id);
        return jdbcTemplate.queryForObject(sql, namedParameters, String.class);
    }
    
    @Override
    public List<Singer> findAllWithAlbums() {
        String sql = "select s.id, s.first_name, s.last_name, s.birth_date, a.id as album_id, a.title, a.release_date "
                     + "from singer s left join album a on s.id = a.singer_id";
        return jdbcTemplate.query(sql, new SingerWithAlbumsExtractor());
    }
    
    @Override
    public void update(Singer singer) {
        String sql = "update singer set first_name= :first_name where id= :id";
        Map<String,  Object> parameters = new HashMap<>();
        parameters.put("id", singer.getId());
        parameters.put("first_name",singer.getFirstName());
        jdbcTemplate.update(sql, parameters);
        
    }
    
    @Override
    public List<Singer> findAll() {
        return jdbcTemplate.query("select * from singer", new SingerMapper());
    }
    
    @Override
    public Long findIdByName(String firstName, String lastName) {
        return null;
    }
    
    private static class SingerWithAlbumsExtractor implements ResultSetExtractor<List<Singer>> {
    
        @Override
        public List<Singer> extractData(ResultSet rs) throws SQLException, DataAccessException {
            Map<Long, Singer> singerMap = new HashMap<>();
            Singer singer;
            while (rs.next()) {
                long id = rs.getLong("id");
                singer = singerMap.get(id);
                if (singer == null) {
                    singer = new Singer();
                    singer.setId(rs.getLong("id"));
                    singer.setFirstName(rs.getString("first_name"));
                    singer.setLastName(rs.getString("last_name"));
                    singer.setBirthDate(rs.getDate("birth_date"));
                    singerMap.put(id, singer);
                }
                long albumId = rs.getLong("album_id");
                if (albumId > 0) {
                    Album album = new Album();
                    album.setId(albumId);
                    album.setSingerId(id);
                    album.setTitle(rs.getString("title"));
                    album.setReleaseDate(rs.getDate("release_date"));
                    singer.addAlbum(album);
                }
            }
            return new ArrayList<>(singerMap.values());
        }
    }
    
    private static class SingerMapper implements RowMapper<Singer> {
    
        @Override
        public Singer mapRow(ResultSet resultSet, int i) throws SQLException {
            System.out.println("int i: " + i);
            Singer singer = new Singer();
            singer.setId(resultSet.getLong("id"));
            singer.setFirstName(resultSet.getString("first_name"));
            singer.setLastName(resultSet.getString("last_name"));
            singer.setBirthDate(resultSet.getDate("birth_date"));
            return singer;
        }
    }
}
