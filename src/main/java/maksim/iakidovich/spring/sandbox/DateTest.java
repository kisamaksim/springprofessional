package maksim.iakidovich.spring.sandbox;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

public class DateTest {
    public static void main(String[] args) throws ParseException {
        SimpleDateFormat fr = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.DEFAULT,DateFormat.DEFAULT, Locale.FRANCE);
        SimpleDateFormat eng = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.DEFAULT,DateFormat.DEFAULT, Locale.US);
        SimpleDateFormat usa = (SimpleDateFormat) DateFormat.getDateTimeInstance(DateFormat.DEFAULT,DateFormat.DEFAULT, Locale.UK);
        SimpleDateFormat german = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmX", Locale.GERMANY);
        List<SimpleDateFormat> simpleDateFormats = Arrays.asList(fr, eng, usa, german);
//        simpleDateFormats.forEach(simpleDateFormat -> simpleDateFormat.applyPattern("yyyy-MM-dd'T'HH:mmX"));
        simpleDateFormats.forEach(simpleDateFormat -> simpleDateFormat.applyPattern("yyyy-MM-dd"));
        simpleDateFormats.forEach(simpleDateFormat -> simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC")));

//        Date date = new Date(1361805900000L);
//        System.out.println(date);
        Date date = fr.parse("2001-07-04T13:00-03:00");
        Date parse = german.parse("2001-07-04T13:00-03:00");
        System.out.println(date);
        String test = null;
        for (SimpleDateFormat simpleDateFormat: simpleDateFormats) {
//            System.out.println(simpleDateFormat.getTimeZone());
            test = simpleDateFormat.format(date);
            System.out.println(test);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-MM-dd HH:mm a");
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("yyyy-MMMM-dd HH:mm a");
        SimpleDateFormat simpleDateFormat3 = new SimpleDateFormat("yyyy-MMMM-dd HH:mm a", Locale.GERMANY);
        Date parse1 = simpleDateFormat.parse(test);
        System.out.println();
        
        
        
        new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
    
    }
}
