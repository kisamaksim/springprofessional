package maksim.iakidovich.spring.sandbox.injectionexperements;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class A implements Inter {
    
    @Value("${test}")
    private String a;
    private String ba;
    private String c;
    private String d;
    
    @Autowired
    private B b;
    
    @Autowired
    private C ca;
    
    @PostConstruct
    public void init() {
        System.out.println("phase 2");
        System.out.println(b);
        System.out.println(ca);
    }
    
    
//    public A(B b) {
//        this.b = b;
//        System.out.println("phase 1");
//        System.out.println(b);
//        System.out.println(ca);
//    }
}
