package maksim.iakidovich.spring.sandbox.injectionexperements;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan("maksim.iakidovich.spring.sandbox.injectionexperements")
@PropertySource("classpath:sandbox/sandbox.property")
public class Config {
    
    
    public static void main(String[] args) {
//        C.bo();
//        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        Inter bean = context.getBean("a", Inter.class);
        System.out.println();
        
    }
}
