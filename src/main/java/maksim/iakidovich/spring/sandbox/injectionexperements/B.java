package maksim.iakidovich.spring.sandbox.injectionexperements;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class B {
    @Autowired
    private A a;
    
//    public B(A a) {
//        this.a = a;
//    }
}
