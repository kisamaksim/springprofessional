package maksim.iakidovich.spring.sandbox;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

import maksim.iakidovich.spring.ch12.services.SingerService;

@Configuration
public class RmiConfig {
    
    @Bean
    public SingerService singerService() {
        HttpInvokerProxyFactoryBean httpInvokerProxyFactoryBean = new HttpInvokerProxyFactoryBean();
        httpInvokerProxyFactoryBean.setServiceInterface(SingerService.class);
        httpInvokerProxyFactoryBean.setServiceUrl("http://localhost:9797/testapp/invoker/httpInvoker/singerService");
        httpInvokerProxyFactoryBean.afterPropertiesSet();
        return (SingerService) httpInvokerProxyFactoryBean.getObject();
    }
    
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(RmiConfig.class);
        SingerService bean = context.getBean(SingerService.class);
        System.out.println(bean.findAll());
    }
    
}
