package maksim.iakidovich.spring.ch14.groovy

class Singer {
    def name;
    def lastName;

    @Override
    public String toString() {
        return "Singer{" + "name=" + name + ", lastName=" + lastName + '}';
    }
}

Singer singer = new Singer(name: 20, lastName: "Iakidovich")
Singer singer1 = new Singer(name: "Maksim", lastName: "Ia")

println singer
println singer1

println singer.name + 100
println singer1.name + 100
