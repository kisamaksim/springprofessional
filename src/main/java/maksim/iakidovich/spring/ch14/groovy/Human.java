package maksim.iakidovich.spring.ch14.groovy;

import org.joda.time.DateTime;

import lombok.Data;

@Data
public class Human {
    private Long id;
    private String firstName;
    private String lastName;
    private DateTime birthDate;
    private String ageCategory;
    
}
