package maksim.iakidovich.spring.ch14.groovy

def names = ["maksim", "polina"]

names.each {println "Hello: " + it}

def hashM = ["a": 100, "b": 150]
Closure test = {key, value -> hashM[key] = value * 10}
hashM.each test
println hashM
def k = ["c": 10, "r": 20]
k.each test
println k

