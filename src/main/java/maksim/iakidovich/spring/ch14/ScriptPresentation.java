package maksim.iakidovich.spring.ch14;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScriptPresentation {
    public static void main(String[] args) {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine javaScript = scriptEngineManager.getEngineByName("JavaScript");
        try {
            javaScript.eval("print('Hello JavaScript in Java')");
        } catch (ScriptException e) {
            log.error("error: {}", e.getMessage());
        }
    }
}
