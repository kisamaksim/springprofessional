package maksim.iakidovich.spring.ch5.sub4point6;

public class Hulk {
    
    public void destroy() {
        System.out.println("HULK DESTROY");
    }
    
    public void destroy(String what) {
        System.out.println("HULK DESTROY " + what);
    }
    
    public void f() {
        System.out.println("f");
    }
    
    public void g() {
        System.out.println("g");
    }
}
