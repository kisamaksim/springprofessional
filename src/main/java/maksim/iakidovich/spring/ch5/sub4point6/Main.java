package maksim.iakidovich.spring.ch5.sub4point6;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.NameMatchMethodPointcut;

import maksim.iakidovich.spring.ch5.sub4point4.SImpleAdvice;

public class Main {
    public static void main(String[] args) {
        Hulk hulk = new Hulk();
        NameMatchMethodPointcut pointcut = new NameMatchMethodPointcut();
        pointcut.addMethodName("destroy");
        pointcut.addMethodName("g");
    
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor(pointcut, new SImpleAdvice());
    
        ProxyFactory proxyFactory = new ProxyFactory(hulk);
        proxyFactory.addAdvisor(advisor);
    
        Hulk proxy = (Hulk) proxyFactory.getProxy();
        proxy.destroy();
        proxy.destroy("cup of tea");
        proxy.g();
        proxy.f();
    
    }
}
