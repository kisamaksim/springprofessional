package maksim.iakidovich.spring.ch5.sub2point2;

import org.springframework.aop.framework.ProxyFactory;

public class Main {
    public static void main(String[] args) {
        Agent agent = new Agent();
    
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(new AgentDecorator());
        proxyFactory.setTarget(agent);
    
        Agent agentProxy = (Agent) proxyFactory.getProxy();
        
        agentProxy.speak();
        System.out.println();
        agentProxy.f();
    }
}
