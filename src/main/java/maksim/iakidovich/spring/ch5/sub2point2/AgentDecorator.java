package maksim.iakidovich.spring.ch5.sub2point2;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class AgentDecorator implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation methodInvocation) throws Throwable {
        System.out.print("James ");
    
        Object proceed = methodInvocation.proceed();
        System.out.print("!");
        
        return proceed;
    }
}
