package maksim.iakidovich.spring.ch5.sub8point2;

public class Guitarist {
    
    public void sing() {
        System.out.println("SING");
    }
    
    public void talk() {
        System.out.println("TALK");
    }
}
