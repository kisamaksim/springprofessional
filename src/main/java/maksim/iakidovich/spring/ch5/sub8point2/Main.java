package maksim.iakidovich.spring.ch5.sub8point2;

import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext("classpath:ch5/aop-config.xml");
    
        Documentarist docOne = context.getBean("docOne", Documentarist.class);
        
        docOne.work();
    }
}
