package maksim.iakidovich.spring.ch5.sub8point2;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

public class Adv implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println("Executing: " + method.getName());
    }
}
