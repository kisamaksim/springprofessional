package maksim.iakidovich.spring.ch5.sub8point2;

import lombok.Setter;

@Setter
public class Documentarist {
    private Guitarist guitarist;
    
    public void work() {
        guitarist.sing();
        guitarist.talk();
    }
}
