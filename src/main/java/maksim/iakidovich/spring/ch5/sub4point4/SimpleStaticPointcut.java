package maksim.iakidovich.spring.ch5.sub4point4;

import java.lang.reflect.Method;

import org.springframework.aop.ClassFilter;
import org.springframework.aop.support.StaticMethodMatcherPointcut;

public class SimpleStaticPointcut extends StaticMethodMatcherPointcut {
    @Override
    public ClassFilter getClassFilter() {
        return cls -> cls == GoodBoy.class;
    }
    
    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        
        return "doSomething".equals(method.getName());
    }
}
