package maksim.iakidovich.spring.ch5.sub4point4;

import org.aopalliance.aop.Advice;
import org.springframework.aop.Advisor;
import org.springframework.aop.Pointcut;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

public class Main {
    public static void main(String[] args) {
        GoodBoy goodBoy = new GoodBoy();
        BadBoy badBoy = new BadBoy();
    
        Advice advice = new SImpleAdvice();
        Pointcut pointcut = new SimpleStaticPointcut();
        Advisor advisor = new DefaultPointcutAdvisor(pointcut, advice);
    
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvisor(advisor);
        proxyFactory.setTarget(goodBoy);
        GoodBoy proxyGood = (GoodBoy) proxyFactory.getProxy();
    
        proxyGood.doSomething();
        
        proxyFactory.setTarget(badBoy);
        
        Boy proxyBad = (Boy) proxyFactory.getProxy();
        proxyBad.doSomething();
        
    }
}
