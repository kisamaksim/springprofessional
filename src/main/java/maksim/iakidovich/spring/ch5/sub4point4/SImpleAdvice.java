package maksim.iakidovich.spring.ch5.sub4point4;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class SImpleAdvice implements MethodInterceptor {
    @Override
    public Object invoke(MethodInvocation invocation) throws Throwable {
        System.out.println("<<<<<");
    
        Object proceed = invocation.proceed();
    
        System.out.println(">>>>>");
        
        return proceed;
    }
}
