package maksim.iakidovich.spring.ch5.sub4point4;

public class GoodBoy implements Boy{
    @Override
    public void doSomething() {
        System.out.println("I am GOOD!");
    }
    
    public void test() {
        System.out.println("");
    }
}
