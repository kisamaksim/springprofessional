package maksim.iakidovich.spring.ch5.sub6point1;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.ControlFlowPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import maksim.iakidovich.spring.ch5.sub3point6.Singer;
import maksim.iakidovich.spring.ch5.sub4point4.SImpleAdvice;

public class Main {
    public static void main(String[] args) {
        Singer singer = new Singer();
        ControlFlowPointcut pointcut = new ControlFlowPointcut(Main.class, "test");
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor(pointcut, new SImpleAdvice());
    
        ProxyFactory proxyFactory = new ProxyFactory(singer);
        proxyFactory.addAdvisor(advisor);
    
        Singer proxy = (Singer) proxyFactory.getProxy();
        proxy.sing();
    
        Main main = new Main();
        main.test(proxy);
    }
    
    private void test(Singer singer) {
        System.out.println("test from Main");
        singer.sing();
        singer.song();
    }
}
