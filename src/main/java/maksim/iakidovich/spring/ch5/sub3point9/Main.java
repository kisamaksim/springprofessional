package maksim.iakidovich.spring.ch5.sub3point9;

import org.springframework.aop.framework.ProxyFactory;

public class Main {
    public static void main(String[] args) {
        Problems problems = new Problems();
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(new AfterTh());
        proxyFactory.setTarget(problems);
    
        Problems proxy = (Problems) proxyFactory.getProxy();
        try {
            proxy.pro();
        } catch (IllegalArgumentException ignored) {
        
        }
        try {
            proxy.prob("maksim",1,99.0);
        } catch (NullPointerException ignored) {
        
        }
    }
}
