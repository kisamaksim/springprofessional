package maksim.iakidovich.spring.ch5.sub3point9;

public class Problems {
    
    public void pro() {
        throw new IllegalArgumentException();
    }
    
    public void prob(String name, int i, double j) {
        throw new NullPointerException();
    }
    
}
