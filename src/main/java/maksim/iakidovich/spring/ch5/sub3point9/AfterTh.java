package maksim.iakidovich.spring.ch5.sub3point9;

import java.lang.reflect.Method;
import java.util.Arrays;

import org.springframework.aop.ThrowsAdvice;

public class AfterTh implements ThrowsAdvice {
    
    public void afterThrowing(IllegalArgumentException ex) {
        System.out.println("****");
        System.out.println("I caught: " + ex);
        System.out.println("****");
    }
    
    public void afterThrowing(Method method, Object[] args, Object target, NullPointerException ex) {
        System.out.println("****");
        System.out.println("I caught: " + ex);
        System.out.println("In class: " + target.getClass().getSimpleName());
        System.out.println("On method: " + method.getName());
        System.out.println("With args: " + Arrays.toString(args));
        System.out.println("****");
    }
}
