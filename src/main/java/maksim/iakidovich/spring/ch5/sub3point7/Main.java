package maksim.iakidovich.spring.ch5.sub3point7;

import org.springframework.aop.framework.ProxyFactory;

import maksim.iakidovich.spring.ch5.sub3point6.Singer;

public class Main {
    public static void main(String[] args) {
        Singer singer = new Singer();
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(new AfterReturningAdviceM());
        proxyFactory.setTarget(singer);
        Singer proxySinger = (Singer) proxyFactory.getProxy();
        
        proxySinger.sing();
    }
}
