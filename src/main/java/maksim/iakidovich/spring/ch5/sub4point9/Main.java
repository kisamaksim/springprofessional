package maksim.iakidovich.spring.ch5.sub4point9;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;

import maksim.iakidovich.spring.ch5.sub4point4.SImpleAdvice;

public class Main {
    public static void main(String[] args) {
        AnnotationSinger annotationSinger = new AnnotationSinger();
        Class<?>[] interfaces = annotationSinger.getClass().getInterfaces();
    
        AnnotationMatchingPointcut pointcut = AnnotationMatchingPointcut.forMethodAnnotation(ReqAdvice.class);
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor(pointcut, new SImpleAdvice());
    
        ProxyFactory proxyFactory = new ProxyFactory(annotationSinger);
        proxyFactory.addAdvisor(advisor);
    
        AnnotationSinger proxy = (AnnotationSinger) proxyFactory.getProxy();
        
//        proxy.sing();
//        proxy.f();
//        proxy.g();
    
    
        AnnotationMatchingPointcut pointcut1 = AnnotationMatchingPointcut.forClassAnnotation(ReqClassAdvice.class);
        advisor.setPointcut(pointcut1);
        AnnotationSinger proxy1 = (AnnotationSinger) proxyFactory.getProxy();
        proxy1.g();
        proxy1.sing();
        proxy1.f();
    }
}
