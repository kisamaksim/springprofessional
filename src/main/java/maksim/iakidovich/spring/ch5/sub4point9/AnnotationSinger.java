package maksim.iakidovich.spring.ch5.sub4point9;

@ReqClassAdvice
public class AnnotationSinger {
    
    @ReqAdvice
    public void sing() {
        System.out.println("simple song");
    }
    
    public void f() {
        System.out.println("f isn't song");
    }
    
    @ReqAdvice
    public void g() {
        System.out.println("gone, see you later");
    }
}
