package maksim.iakidovich.spring.ch5.sub3point6;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

public class BeforeAdvice implements MethodBeforeAdvice {
    @Override
    public void before(Method method, Object[] objects, Object o) throws Throwable {
        System.out.println("befor method: " + method.getName() + " go blablabla");
        if (method.getName().equals("f")) {
            objects[0] = "Before Advice";
        }
    }
}
