package maksim.iakidovich.spring.ch5.sub3point6;

public class Singer {
    private String lyrics = "Hello, it's me";
    
    public void sing() {
        System.out.println(lyrics);
    }
    
    public void sing2() {
        System.out.println("sing222");
    }
    
    public void song() {
        System.out.println("I am song");
    }
    
    public void f(String string) {
        System.out.println(string);
    }
}
