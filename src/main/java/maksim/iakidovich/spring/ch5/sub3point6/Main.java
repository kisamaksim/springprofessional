package maksim.iakidovich.spring.ch5.sub3point6;

import org.springframework.aop.framework.ProxyFactory;

public class Main {
    public static void main(String[] args) {
        Singer singer = new Singer();
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvice(new BeforeAdvice());
        proxyFactory.setTarget(singer);
        
        Singer proxy = (Singer) proxyFactory.getProxy();
        proxy.sing();
        proxy.song();
        proxy.f("Maksim");
    }
}
