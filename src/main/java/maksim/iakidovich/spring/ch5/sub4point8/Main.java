package maksim.iakidovich.spring.ch5.sub4point8;

import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import maksim.iakidovich.spring.ch5.sub3point6.Singer;
import maksim.iakidovich.spring.ch5.sub4point4.SImpleAdvice;
import maksim.iakidovich.spring.ch5.sub4point5.SimpleDynamicPointcut;

public class Main {
    public static void main(String[] args) {
        Singer singer = new Singer();
        AspectJExpressionPointcut aspectJExpressionPointcut = new AspectJExpressionPointcut();
        aspectJExpressionPointcut.setExpression("execution(* sing*(..))");
    
        DefaultPointcutAdvisor advisor = new DefaultPointcutAdvisor(aspectJExpressionPointcut, new SImpleAdvice());
    
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addAdvisor(advisor);
        proxyFactory.setTarget(singer);
        
        Singer proxy = (Singer) proxyFactory.getProxy();
        proxy.sing();
        proxy.sing2();
        proxy.f("maksim");
    
    
    }
}
