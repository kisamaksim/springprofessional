package maksim.iakidovich.spring.ch5.sub7point2;

import org.springframework.aop.framework.ProxyFactory;

public class Main {
    public static void main(String[] args) {
        Contact contact = new Contact();
        contact.setName("Maksim");
    
        IsModifiedAdvisor isModifiedAdvisor = new IsModifiedAdvisor();
    
        ProxyFactory proxyFactory = new ProxyFactory(contact);
        proxyFactory.addAdvisor(isModifiedAdvisor);
        proxyFactory.setOptimize(true);
        Contact proxy = (Contact) proxyFactory.getProxy();
        
        proxy.setName("Maksim");
        
        IsModified modified = (IsModified) proxy;
        System.out.println(modified.isModified());
        
        proxy.setName("Polina");
        System.out.println(modified.isModified());
        
        proxy.setEmail("blabla");
        System.out.println(modified.isModified());
        proxy.setEmail("blabla");
        System.out.println(modified.isModified());
        
    }
}
