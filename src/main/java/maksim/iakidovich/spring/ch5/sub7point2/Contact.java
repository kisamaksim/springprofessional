package maksim.iakidovich.spring.ch5.sub7point2;

import lombok.Data;

@Data
public class Contact {
    private String name;
    private String phoneNumber;
    private String email;
}
