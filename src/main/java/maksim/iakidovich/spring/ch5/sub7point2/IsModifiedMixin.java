package maksim.iakidovich.spring.ch5.sub7point2;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.support.DelegatingIntroductionInterceptor;

public class IsModifiedMixin extends DelegatingIntroductionInterceptor implements IsModified {
    
    private boolean isModified;
    private Map<Method, Method> methodCash = new HashMap<>();
    
    @Override
    public boolean isModified() {
        return isModified;
    }
    
    @Override
    public Object invoke(MethodInvocation mi) throws Throwable {
        if (!isModified) {
            if ((mi.getMethod().getName().startsWith("set")) && (mi.getArguments().length == 1)) {
                Method getter = getGetter(mi.getMethod());
                
                if (getter != null) {
                    Object newVal = mi.getArguments()[0];
                    Object oldValue = getter.invoke(mi.getThis());
                    
                    if ((newVal == null) && (oldValue == null)) {
                        isModified = false;
                    } else if ((newVal == null) && (oldValue != null)) {
                        isModified = true;
                    } else if ((newVal != null) && (oldValue == null)) {
                        isModified = true;
                    } else {
                        isModified = !newVal.equals(oldValue);
                    }
                }
            }
        }
        return super.invoke(mi);
    }
    
    private Method getGetter(Method setter) {
        Method getter = methodCash.get(setter);
        if (getter != null) {
            return getter;
        }
    
        String getterName = setter.getName().replaceFirst("set", "get");
        
        try {
            getter = setter.getDeclaringClass().getDeclaredMethod(getterName);
            methodCash.put(setter, getter);
            return getter;
        } catch (NoSuchMethodException ignored) {
            return null;
        }
    }
}
