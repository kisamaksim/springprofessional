package maksim.iakidovich.spring.ch5.sub7point2;

public interface IsModified {
    boolean isModified();
}
