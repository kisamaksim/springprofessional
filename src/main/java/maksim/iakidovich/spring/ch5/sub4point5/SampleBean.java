package maksim.iakidovich.spring.ch5.sub4point5;

import lombok.Data;

@Data
public class SampleBean implements FakeInterface{
    public void foo(int x) {
        System.out.println("foo with " + x);
    }
    
    public void g() {
        System.out.println("I Am GGG");
    }
}
