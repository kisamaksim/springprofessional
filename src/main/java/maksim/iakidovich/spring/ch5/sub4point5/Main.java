package maksim.iakidovich.spring.ch5.sub4point5;

import org.springframework.aop.Advisor;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.support.DefaultPointcutAdvisor;

import maksim.iakidovich.spring.ch5.sub3point6.BeforeAdvice;
import maksim.iakidovich.spring.ch5.sub4point4.SImpleAdvice;

public class Main {
    public static void main(String[] args) {
        SampleBean sampleBean = new SampleBean();
    
        Advisor advisor = new DefaultPointcutAdvisor(new SimpleDynamicPointcut(), new SImpleAdvice());
    
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.setTarget(sampleBean);
//        proxyFactory.setInterfaces(FakeInterface.class);
        proxyFactory.addAdvisor(advisor);
//        proxyFactory.setFrozen(true); если установлено, то ловим эксепшен если пытаемся добавить совета
    
        SampleBean proxy = (SampleBean) proxyFactory.getProxy();
//        FakeInterface proxy = (FakeInterface) proxyFactory.getProxy();
        proxy.foo(1);
        proxy.foo(12);
        proxy.foo(100);

        proxy.g();
        proxy.g();
        
        proxyFactory.addAdvice(new BeforeAdvice());
        
        proxy.foo(12);
        proxy.g();
    }
}
