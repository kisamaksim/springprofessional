package maksim.iakidovich.spring.ch5.sub4point5;

import java.lang.reflect.Method;

import org.springframework.aop.support.DynamicMethodMatcherPointcut;

public class SimpleDynamicPointcut extends DynamicMethodMatcherPointcut {
    @Override
    public boolean matches(Method method, Class<?> targetClass) {
        System.out.println("Static check for: " + method.getName());
        return "foo".equals(method.getName());
    }
    
    @Override
    public boolean matches(Method method, Class<?> targetClass, Object... args) {
        System.out.println("Dynamic check for: " + method.getName());
        int arg = (int) args[0];
        return arg != 100;
    }
}
