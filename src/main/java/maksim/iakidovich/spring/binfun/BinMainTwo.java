package maksim.iakidovich.spring.binfun;

public class BinMainTwo {
    
    public static void main(String[] args) {
        int[] ar = {1,2,3,4,5,6,7, 100, 200, 245};
        System.out.println(binSearch(ar, 100));
    }
    
    private static int binSearch(int[] array, int element) {
        int high = array.length - 1;
        int low = 0;
        int mid = -1;
        
        while (low <= high) {
            mid = (low + high) / 2;
            if (array[mid] == element) {
                return mid;
            }
            if (array[mid] < element) {
                low = mid + 1;
            } else {
                high = mid - 1;
            }
        }
        return mid;
    }
}
