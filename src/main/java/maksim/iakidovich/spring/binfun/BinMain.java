package maksim.iakidovich.spring.binfun;

public class BinMain {
    public static void main(String[] args) {
        BinMain binMain = new BinMain();
        int[] arr = {1, 3, 5, 124, 324, 456, 540, 600, 1056, 10145};
        System.out.println(binMain.binarySearch(arr, 124));
        System.out.println(binMain.binarySearch(arr, 1));
        System.out.println(binMain.binarySearch(arr, 3));
        System.out.println(binMain.binarySearch(arr, 1056));
    }
    
    public int binarySearch(int[] array, int value) {
        int upper = array.length - 1;
        int low = 0;
        int middle = 0;
        
        while (low <= upper) {
            middle = (low + upper) / 2;
            
            if (array[middle] == value) {
                return middle;
            }
            
            if (array[middle] > value) {
                upper = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return middle;
    }
}
